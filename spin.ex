defmodule Spin do
  def spin_words(message) do
    message
    |> String.split(" ")
    |> Enum.map(&spin/1)
    |> Enum.join(" ")
  end

  def spin(word) do
    case String.length(word) do
      n when n >= 5 -> String.reverse(word)
      _ -> word
    end
  end
end

spin_words = &Spin.spin_words/1
IO.puts(spin_words.("Have fun!"))
IO.puts(spin_words.("Welcome"))
IO.puts(spin_words.("Hey fellow warriors"))
